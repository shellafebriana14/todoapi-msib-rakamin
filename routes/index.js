const express       = require('express')
const path          = require('path')
const controller    = require('../controllers/title')
const methodOverride= require('method-override')
const router        = express.Router()

router.use(methodOverride('_method'))
router.get('/', controller.index)
router.get('/detail/:id', controller.detail)
router.get('/add', (req, res) => {
    res.render('create', {
        title : 'Add Todo',
        layout : 'layouts/main-layouts'
    })
})
router.post('/', controller.create)
router.post('/restore/:id', controller.restore)
router.delete('/delete/:id', controller.delete)
module.exports = router