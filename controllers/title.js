const { Title } = require('../models')

class TitleController{
    static index(req, res, next){
        Title.findAndCountAll({order: [['id', 'DESC']], paranoid: false})
            .then( data => {
                res.render('index', {
                    title : 'Home',
                    layout : 'layouts/main-layouts',
                    data : {
                        "currentPageSize" : data.rows.length,
                        "titles" : data.rows
                    }
                })
            })
            .catch(err => {
                res.render('error-page', {
                    title : 'Error Page!',
                    layout : 'layouts/main-layout',
                    status : 500,
                    message : 'Something went wrong',
                    error : err
                })
            })
    }
    
    static detail(req, res, next){
        Title.findOne({
            where  : {
                id : req.params.id
            }
        })
        .then( data =>{
            if(!data){
                res.render('error-page', {
                    title : 'Error Page!',
                    layout : 'layouts/main-layouts',
                    status : 404,
                    message : 'Data not found',
                    error :''
                })
            }else{
                res.render('detail', {
                    title : 'Detail Todo',
                    layout : 'layouts/main-layouts',
                    data
                })
            }
        })
        .catch(err => {
            res.render('error-page', {
                title : 'Error Page!',
                layout : 'layouts/main-layout',
                status : 500,
                message : 'Something went wrong',
                error : err
            })
        })
    }

    static create(req,res,next){
        const info = {
            title : req.body.title,
            delete : 0,
            createdAt : new Date()
        }
        Title.create(info)
        .then(res.redirect('/'))
        .catch(err => {
            res.render('error-page', {
                title : 'Error Page!',
                layout : 'layouts/main-layouts',
                status : 500,
                message : 'Something went wrong',
                error : err
            })
        })
    }

    static delete(req, res, next){
        Title.destroy({
            where: {
              id: req.params.id
            }
          })
        .then(res.redirect('/'))
        .catch(err => {
            res.render('error-page', {
                title : 'Error Page!',
                layout : 'layouts/main-layouts',
                status : 500,
                message : 'Something went wrong',
                error : err
            })
        })
    }

    static restore(req, res,next){
        Title.restore({
            where: {
              id: req.params.id
            }
        })
        .then(res.redirect('/'))
        .catch(err => {
            res.render('error-page', {
                title : 'Error Page!',
                layout : 'layouts/main-layouts',
                status : 500,
                message : 'Something went wrong',
                error : err
            })
        })
    }
}

module.exports = TitleController