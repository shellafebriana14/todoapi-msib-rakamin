'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    const addTodo = [
      {
        title : 'Export income periode di mixradius',
        createdAt : new Date()
      },
      {
        title : 'Follow up admin tf cash ke rek PT',
        createdAt : new Date()
      },
      {
        title : 'Rekonsiliasi dan mutasi invoice pemasangan',
        createdAt : new Date()
      },
      {
        title : 'Buat otorisasi pembayaran',
        createdAt : new Date()
      },
      {
        title : 'Cek pettycash',
        createdAt : new Date()
      },
      {
        title : 'Cek data transfer di mutasi rek PT',
        createdAt : new Date()
      },
      {
        title : 'Belajar module front end React JS',
        createdAt : new Date()
      },
      {
        title : 'Diskusi design ERD dengan kelompok 9',
        createdAt : new Date()
      },
      {
        title : 'Isi logbook mingguan',
        createdAt : new Date()
      },
      {
        title : 'Cicil laporan akhir',
        createdAt : new Date()
      }
    ]

    await queryInterface.bulkInsert('Titles', addTodo, {
      ignoreDuplicates : true
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Titles', null ,{})
  }
};
